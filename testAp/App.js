import React, { Component } from 'react';
import { 
  View, 
  Text,
  Image, 
  ScrollView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from 'react-native';

import * as axios from 'axios';
import ImagePicker from 'react-native-image-picker';


class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      imageList: [],
      isLoading: false,
      isUploading: false,
    };
  }

  componentDidMount(){
    this.fetchImages();
  }

  fetchImages = () => {
    this.setState({
      isLoading: true,
    },()=> {
      axios.post('http://localhost:3333/web/fetchImages')
      .then((response)=> {
        this.setState({
          isLoading: false,
          imageList: response.data,
        })
      })
      .catch((error)=> {
        this.setState({
          isLoading: false,
        },()=> {
          console.warn(error);
        })
      })
    })
  }

  showOptions = () => {

    const options = {
      title: 'Select Avatar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // const source = { uri: response.uri };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
        // this.setState({
        //   avatarSource: source,
        // });

        // console.log(response.uri);

        // console.log({
        //     id: this.state.imageList[this.state.imageList.length],
        //     imageName: "test "+this.state.imageList[this.state.imageList.length],
        //     imageURI: response.uri
        // });

        this.setState({
          isLoading: true,
        },()=> {
          axios.post('http://localhost:3333/web/storeImage',{
              id: this.state.imageList.length,
              imageName: "test "+this.state.imageList.length,
              imageURI: response.data
          })
          .then((response)=> {

            this.setState({
              isUploading: false,
            },()=> {
              this.fetchImages()
            })            

          })
          .catch((error)=> {
            this.setState({
              isUploading: false,
            },()=> {
              console.warn(error);
            })
          })

        })
        
      }
    });
  }

  render() {
    return (
      <View style={{flex: 1, }}>

      <ScrollView contentContainerStyle={{flex:1, alignItems: 'center', marginTop: 30, paddingBottom: 200,  }}>
        {
            this.state.imageList.length === 0 ? (
              <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  <ActivityIndicator size="large" color="#27ae60" />
              </View>
            ): (
              <View />
            )
        }

        {
            this.state.isUploading ? (
              <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  <ActivityIndicator size="large" color="#27ae60" />
                  <Text>Uploading Please wait...</Text>
              </View>
            ): (
              <View />
            )
        }


      {
        this.state.imageList.map((item, index)=> {
          return(
            <View key={index} style={{width: '90%', height: 200, backgroundColor: '#2c3e50', borderRadius: 5, marginTop: 10, marginBottom: 10, }}>
              <Image source={{uri: item.imageURI}} style={{flex: 1, borderRadius: 5, }} />
            </View>
          )
        })
      }
      </ScrollView>
          


        <TouchableOpacity 
        onPress={()=> this.showOptions()}
        style={{position: 'absolute', bottom :20, right: 15, width: 100, height: 100, justifyContent: 'center', alignItems: 'center', backgroundColor: '#c0392b', borderRadius: 50, }}>
          <Text style={{fontSize: 50, color: '#ffffff', }}>+</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default App;
